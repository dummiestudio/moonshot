extends KinematicBody

onready var liquid = $ShotGlass/Liquid
onready var shake_tween = $ShakeTween

export(float) var DEFORMATION_INTENSITY = 0.5
export(float) var SHAKING_TIME = 1.0
export(float) var WAVE_SPEED = 20.0

func _ready():
	liquid.set("material/0",liquid.get("material/0").duplicate())
	

func get_liquid_material():
	return liquid.get("material/0") 
	
func shake(duration, dir):
	var liquid_material = get_liquid_material()
	liquid_material.set_shader_param("deformation_dir", Vector2(dir.x, dir.z))
	shake_tween.interpolate_property(liquid_material, "shader_param/deformation_factor", 0.0, DEFORMATION_INTENSITY, duration,Tween.TRANS_CUBIC, Tween.EASE_IN_OUT)
	
	shake_tween.start()
	
	yield(shake_tween, "tween_all_completed")
	
	liquid_material.set_shader_param("shader_param/wave_speed", WAVE_SPEED)
	shake_tween.interpolate_property(liquid_material, "shader_param/deformation_factor", DEFORMATION_INTENSITY, 0.0, SHAKING_TIME, Tween.TRANS_CUBIC, Tween.EASE_IN_OUT)
	shake_tween.interpolate_property(liquid_material, "shader_param/shake_time", 0.0, SHAKING_TIME, SHAKING_TIME,Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	
	shake_tween.start()
	
	yield(shake_tween, "tween_all_completed")
	
	liquid_material.set_shader_param("deformation_dir", Vector2.ZERO)
	liquid_material.set_shader_param("shader_param/wave_speed", 0.0)
	
