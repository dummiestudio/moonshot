extends Spatial

# Structural class to represent a move (used in undo feature)
class Move:
	# original position of the block
	var from
	
	# final position of the block
	var to
	
	# the block that moved
	var block
	
	# the character that moved the block
	var character
	
	func _init(character, block, from, to):
		self.from = from
		self.to = to
		self.block = block
		self.character = character
		

# constants used to analyze GridMap's collisions and targets 
export(float) var move_duration = 0.75

const WALL_CELL = "Wall"
const RED_SQUARE = "RedSquare"

# tween used for smooth push and undo animation
onready var move_tween = $MoveTween

# the GridMap itself
onready var grid = $GridMap

# the meshlib (used to get the grid cell name)
onready var mesh_lib = grid.mesh_library

# The end game popin (must be hidden at the begining)
onready var center_container = $CanvasLayer/CenterContainer

# the restart/undo buttons popin
onready var button_container = $CanvasLayer/ButtonsContainer

# stores the target's count (used to check for victory)
var count_red_square = 0

# the move stack (used for undo feature)
var move_stack = []

func _ready():
	# initial count of the targets
	for m in grid.get_used_cells():
		if m.y == -1:
			if get_cell_name(m.x, m.y, m.z) == RED_SQUARE:
				count_red_square += 1

# a method to freeze playable characters during animations
func freeze_characters():
	for c in get_tree().get_nodes_in_group("Character"):
		# the freeze method is implemented in the Character's script
		c.freeze()

# unfreezes all chracters when animation finishes
func unfreeze_characters():
	for c in get_tree().get_nodes_in_group("Character"):
		# the freeze method is implemented in the Character's script
		c.unfreeze()
		
func get_cell_name(x, y, z):
	return mesh_lib.get_item_name(grid.get_cell_item(x, y, z))
		
func _on_Character_pushed_something(character, collider):
	# you cannot push anything during animations and you can only push blocks
	if move_tween.is_active() or not collider.is_in_group("Block"):
		return
		
	# determine the push direction from the movement
	var push_dir = - character.global_transform.basis.z
	if abs(push_dir.z) > abs(push_dir.x):
		push_dir.z = sign(push_dir.z)
		push_dir.x = 0
	else:
		push_dir.z = 0
		push_dir.x = sign(push_dir.x)
		
	
	# get collider global position (origin)
	var collider_pos = collider.global_transform.origin
	
	# get the initial square position in map coordinate
	var map_from = grid.world_to_map(collider_pos)
	
	# get the destination position in map coordinate
	var map_to = map_from + push_dir
	
	# if there is wall where you want to push, don't push
	if get_cell_name(map_to.x, map_to.y, map_to.z) == WALL_CELL:
		return
		
	# get all the blocks...
	var blocks = get_tree().get_nodes_in_group("Block")
		
	# then check if there is any
	for n in blocks:
		var map_pos = grid.world_to_map(n.global_transform.origin)
		if map_to == map_pos:
			return
		
	# freeze characters during animation
	freeze_characters()
	
	var new_pos = grid.map_to_world(map_to.x, map_to.y, map_to.z)
	
	move_tween.interpolate_property(collider, "translation", collider_pos, new_pos, move_duration, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	move_tween.start()
	
	collider.shake(move_duration, push_dir)

	# wait until the animation is finished
	yield(move_tween, "tween_all_completed")
	
	# unfreeze characters
	unfreeze_characters()
	
	# add the move on top of the move stack (useful for undo)
	move_stack.push_front(Move.new(character, collider, map_from, map_to))
	
	# count occupied squares
	var check_squares = 0
	
	# count occupied block
	for n in blocks:
		var map_pos = grid.world_to_map(n.global_transform.origin)
		if get_cell_name(map_pos.x, map_pos.y - 1, map_pos.z) == RED_SQUARE:
			check_squares += 1
			
	# if occupied targets count equals the number of targets, it is victory ! :)
	if check_squares == count_red_square:
		center_container.show()
		
		# restart/undo button may be hidden and characters frozen during after winning
		button_container.hide()
		freeze_characters()
			
			
func _on_RestartButton_pressed():
	get_tree().reload_current_scene()


func _on_UndoButton_pressed():
	# no undo if an animation is still active or if no move has been made
	if move_tween.is_active() or move_stack.empty():
		return
		
	# freeze all characters during animation
	freeze_characters()
	
	# get the last move informations
	var move = move_stack.pop_front()
	var character = move.character
	var from_world = grid.map_to_world(move.from.x, move.from.y, move.from.z)
	var to_world = grid.map_to_world(move.to.x, move.to.y, move.to.z)
	var inverse_vec = from_world - to_world
	
	# move the character and the block
	move_tween.interpolate_property(character, "translation", character.transform.origin, from_world + inverse_vec, 0.5, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	move_tween.interpolate_property(move.block, "translation", to_world, from_world, 0.5, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	
	move_tween.start()
	
	# wait for the animation to finish
	yield(move_tween, "tween_all_completed")
	
	# you can now unfreeze characters
	unfreeze_characters()
