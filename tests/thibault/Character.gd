extends KinematicBody

# you can change the speed directly inside the editor
export(float) var SPEED = 5.0

# states of the characters
enum STATES { NORMAL, FREEZE }

# velocity used in move_and_slide
var velocity = Vector3.ZERO

# the collider the character is colliding with, if any
var collider

# a raycast used to know if character is pushing something
onready var raycast = $RayCast

# the push only occurs after a short amount of time (tweak it in the editor)
onready var push_timer = $PushTimer

# to start, the character is in the normal state (controlled by the player)
onready var state = STATES.NORMAL

# it is easier to implement push logic in a more global script, so use a signal
# when you start pushing
signal pushed_something(character, collider)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	match state:
		STATES.NORMAL:
			# get horizontal and vertical inputs
			var input_h = Input.get_action_strength("ui_right") - Input.get_action_strength("ui_left")
			var input_v = Input.get_action_strength("ui_down") - Input.get_action_strength("ui_up")
	
			velocity.x = input_h
			velocity.z = input_v
			
			# "emulates" the next position (without check for collision)
			# it will be used to rotate the player
			var p = global_transform.origin + velocity
			
			if p != global_transform.origin:
				# smart rotation of the player using quaternion. Check this : 
				# https://docs.godotengine.org/en/stable/tutorials/3d/using_transforms.html#interpolating-with-quaternions
				var target_transform = global_transform.looking_at(p, Vector3.UP)
				var current_quat = global_transform.basis.get_rotation_quat()
				var target_quat = target_transform.basis.get_rotation_quat()
				current_quat = current_quat.slerp(target_quat, 10 * delta)
				global_transform.basis = Basis(current_quat)
				global_transform = global_transform.orthonormalized()
			
			
			# we consider pushing only are just in front of the block and translating to it
			if raycast.is_colliding() and collider != raycast.get_collider() and velocity.length() > 0 :
				collider = raycast.get_collider()
				# we don't push right. Instead, we wait until the end of the timer
				if not push_timer.is_stopped():
					push_timer.stop()
				push_timer.start()
			elif (not raycast.is_colliding() or velocity.length() == 0) and not push_timer.is_stopped():
				# we are not pushing anymore, stop the pushing timer 
				push_timer.stop()
				collider = null
				
			# the magic function to slide automatically
			velocity = move_and_slide(velocity * SPEED)
			
			# never go up !!
			velocity.y = 0
			
		
		# freeze state does nothing for now
		STATES.FREEZE:
			pass

func freeze():
	state = STATES.FREEZE

func unfreeze(): 
	state = STATES.NORMAL

func _on_PushTimer_timeout():
	push()
	collider = null
	
func push():
	# let a "map aware" script know that you are pushing
	# It will know what to do with you :) 
	emit_signal("pushed_something", self, collider)
